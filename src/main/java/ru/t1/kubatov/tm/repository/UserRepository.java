package ru.t1.kubatov.tm.repository;

import ru.t1.kubatov.tm.api.repository.IUserRepository;
import ru.t1.kubatov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User delete(User user) {
        if (user == null) return null;
        models.remove(user);
        return user;
    }

    @Override
    public Boolean loginExists(String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean emailExists(String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

    @Override
    public User findByID(String id) {
        for (final User user : models) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

}
