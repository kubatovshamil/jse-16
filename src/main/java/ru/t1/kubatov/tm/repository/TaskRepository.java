package ru.t1.kubatov.tm.repository;

import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        return add(task);
    }

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectID(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
