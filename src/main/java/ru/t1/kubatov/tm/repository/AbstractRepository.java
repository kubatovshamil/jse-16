package ru.t1.kubatov.tm.repository;

import ru.t1.kubatov.tm.api.repository.IRepository;
import ru.t1.kubatov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public void deleteAll() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return findByID(id) != null;
    }

    @Override
    public M findByID(final String id) {
        for (final M model : models) {
            if (id.equals(model.getId())) return model;
        }
        return null;
    }

    @Override
    public M findByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M delete(final M model) {
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M deleteByID(final String id) {
        final M model = findByID(id);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public M deleteByIndex(final Integer index) {
        final M model = findByIndex(index);
        if (model == null) return null;
        return delete(model);
    }

}
