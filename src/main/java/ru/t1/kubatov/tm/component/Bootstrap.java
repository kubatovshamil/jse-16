package ru.t1.kubatov.tm.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.kubatov.tm.api.repository.ICommandRepository;
import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.api.repository.IUserRepository;
import ru.t1.kubatov.tm.api.service.*;
import ru.t1.kubatov.tm.command.AbstractCommand;
import ru.t1.kubatov.tm.command.project.*;
import ru.t1.kubatov.tm.command.system.*;
import ru.t1.kubatov.tm.command.task.*;
import ru.t1.kubatov.tm.command.user.*;
import ru.t1.kubatov.tm.enumerated.Role;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.exception.system.CommandNotSupportedException;
import ru.t1.kubatov.tm.model.Project;
import ru.t1.kubatov.tm.model.Task;
import ru.t1.kubatov.tm.repository.CommandRepository;
import ru.t1.kubatov.tm.repository.ProjectRepository;
import ru.t1.kubatov.tm.repository.TaskRepository;
import ru.t1.kubatov.tm.repository.UserRepository;
import ru.t1.kubatov.tm.service.*;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new ApplicationDeveloperInfoCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserEditCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserViewCommand());
    }

    public void run(String... args) {
        if (processArgument(args)) processCommand("exit");
        initDemoData();
        initLogger();

        System.out.println("Please enter command: ");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                processCommand(command);
                System.out.println();
            } catch (final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> LOGGER_LIFECYCLE.info("*** TASK MANAGER IS SHUTTING DOWN ***")));
    }

    private void initDemoData() {
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("test", "1234", "test@test.test");
        userService.create("user", "1111", "user@user.user");
        projectService.add(new Project("Project1", Status.COMPLETED));
        projectService.add(new Project("Project2", Status.IN_PROGRESS));
        projectService.add(new Project("Project3", Status.IN_PROGRESS));
        projectService.add(new Project("Project4", Status.NOT_STARTED));
        taskService.add(new Task("Task1", "Task1"));
        taskService.add(new Task("Task2", "Task2"));
        taskService.add(new Task("Task3", "Task3"));
        taskService.add(new Task("Task4", "Task4"));
    }

    public void processArgument(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

}