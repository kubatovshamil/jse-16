package ru.t1.kubatov.tm.exception.entity;

public class UserNotFoundException extends AbstractEntityException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
