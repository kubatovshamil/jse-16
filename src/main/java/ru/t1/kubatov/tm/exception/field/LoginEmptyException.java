package ru.t1.kubatov.tm.exception.field;

public class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}
