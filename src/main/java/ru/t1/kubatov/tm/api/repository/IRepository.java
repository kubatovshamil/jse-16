package ru.t1.kubatov.tm.api.repository;

import ru.t1.kubatov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void deleteAll();

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M add(M model);

    boolean existsById(String id);

    M findByID(String id);

    M findByIndex(Integer index);

    M delete(M model);

    M deleteByID(String id);

    M deleteByIndex(Integer index);

}
