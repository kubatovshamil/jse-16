package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    void removeProjects();

    Task unbindTaskFromProject(String projectId, String taskId);

}
