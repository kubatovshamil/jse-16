package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.enumerated.Sort;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {


    Task updateByID(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);


    List<Task> findAll(Sort sort);

}
