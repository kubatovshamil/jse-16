package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.api.repository.IRepository;
import ru.t1.kubatov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {


}
