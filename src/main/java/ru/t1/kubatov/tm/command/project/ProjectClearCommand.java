package ru.t1.kubatov.tm.command.project;

public class ProjectClearCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Delete all Projects.";

    public final static String NAME = "project-delete";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROJECTS]");
        getProjectTaskService().removeProjects();
        System.out.println("[PROJECTS DELETED]");
    }

}
