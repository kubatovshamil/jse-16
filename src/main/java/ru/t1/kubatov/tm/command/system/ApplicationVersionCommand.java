package ru.t1.kubatov.tm.command.system;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    public final static String DESCRIPTION = "Display program version.";

    public final static String NAME = "version";

    public final static String ARGUMENT = "-v";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.16.0");
    }

}
