package ru.t1.kubatov.tm.command.user;

import ru.t1.kubatov.tm.util.TerminalUtil;

public class UserEditCommand extends AbstractUserCommand {

    public final static String DESCRIPTION = "Edit User.";

    public final static String NAME = "user-edit";


    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER EDIT PROFILE]");
        System.out.println("Enter Last Name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter First Name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Midlle Name:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

}
