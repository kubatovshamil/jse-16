package ru.t1.kubatov.tm.command.user;

import ru.t1.kubatov.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    public final static String DESCRIPTION = "Login User.";

    public final static String NAME = "login";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}
