package ru.t1.kubatov.tm.command.project;

import ru.t1.kubatov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Delete Project by ID.";

    public final static String NAME = "project-delete-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        getProjectService().deleteByID(id);
    }

}
