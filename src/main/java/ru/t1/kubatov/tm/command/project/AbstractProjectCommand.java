package ru.t1.kubatov.tm.command.project;

import ru.t1.kubatov.tm.api.service.IProjectService;
import ru.t1.kubatov.tm.api.service.IProjectTaskService;
import ru.t1.kubatov.tm.command.AbstractCommand;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.model.Project;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    protected void showProjectList(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            System.out.printf("%s[%s]. %s \n", index, project.getId(), project);
            index++;
        }
    }

}
