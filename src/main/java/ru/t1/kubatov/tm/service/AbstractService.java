package ru.t1.kubatov.tm.service;

import ru.t1.kubatov.tm.api.repository.IRepository;
import ru.t1.kubatov.tm.api.service.IService;
import ru.t1.kubatov.tm.exception.field.IdEmptyException;
import ru.t1.kubatov.tm.exception.field.IndexIncorrectException;
import ru.t1.kubatov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public M add(M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findByID(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByID(id);
    }

    @Override
    public M findByIndex(Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public M delete(M model) {
        if (model == null) return null;
        return repository.delete(model);
    }

    @Override
    public M deleteByID(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.deleteByID(id);
    }

    @Override
    public M deleteByIndex(Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.deleteByIndex(index);
    }

}
