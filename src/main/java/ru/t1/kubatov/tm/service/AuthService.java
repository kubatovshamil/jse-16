package ru.t1.kubatov.tm.service;

import ru.t1.kubatov.tm.api.service.IAuthService;
import ru.t1.kubatov.tm.api.service.IUserService;
import ru.t1.kubatov.tm.exception.field.LoginEmptyException;
import ru.t1.kubatov.tm.exception.field.PasswordEmptyException;
import ru.t1.kubatov.tm.exception.system.AccessDeniedException;
import ru.t1.kubatov.tm.model.User;
import ru.t1.kubatov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(String login, String password, String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findByID(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}
