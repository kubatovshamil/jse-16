package ru.t1.kubatov.tm.service;

import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.api.service.ITaskService;
import ru.t1.kubatov.tm.enumerated.Sort;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.exception.entity.TaskNotFoundException;
import ru.t1.kubatov.tm.exception.field.*;
import ru.t1.kubatov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        return repository.add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return repository.add(task);
    }


    @Override
    public Task updateByID(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = repository.findByID(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = repository.findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findByID(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectID(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectID(projectId);

    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Task> comparator = (Comparator<Task>) sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

}
