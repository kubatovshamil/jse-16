package ru.t1.kubatov.tm.service;

import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.api.service.IProjectTaskService;
import ru.t1.kubatov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kubatov.tm.exception.entity.TaskNotFoundException;
import ru.t1.kubatov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kubatov.tm.exception.field.TaskIdEmptyException;
import ru.t1.kubatov.tm.model.Project;
import ru.t1.kubatov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findByID(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectID(projectId);
        for (final Task task : tasks) taskRepository.deleteByID(task.getId());
        projectRepository.deleteByID(projectId);
    }

    @Override
    public void removeProjects() {
        final List<Project> projects = projectRepository.findAll();
        for (final Project project : projects) {
            if (project == null) continue;
            removeProjectById(project.getId());
        }
    }

    @Override
    public Task unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findByID(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}
