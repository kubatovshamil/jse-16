package ru.t1.kubatov.tm.model;

import ru.t1.kubatov.tm.api.model.IWBS;
import ru.t1.kubatov.tm.enumerated.Status;

import java.util.Date;

public class Project extends AbstractModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public Project() {

    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Project(final String name, final Status status) {
        this.name = name;
        this.status = status;
    }


    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (description != null && !description.isEmpty()) result += " : " + description;
        if (status != null) result += " : " + Status.toName(status);
        return result;
    }

}
